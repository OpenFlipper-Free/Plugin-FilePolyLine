<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_US">
<context>
    <name>FilePolyLinePlugin</name>
    <message>
        <source>Polygonal Line</source>
        <translation type="vanished">Polygonale Linie</translation>
    </message>
    <message>
        <location filename="../FilePolyLine.cc" line="61"/>
        <location filename="../FilePolyLine.cc" line="65"/>
        <source>Poly-Line files ( *.pol )</source>
        <translation>Poly Linien Dateien</translation>
    </message>
    <message>
        <location filename="../FilePolyLine.cc" line="120"/>
        <source>saveObject : cannot get object id %1 for save name %2</source>
        <translation>saveObject: Kann Objekt Nummer %1 nicht finden um es als %2 zu speichern</translation>
    </message>
    <message>
        <location filename="../FilePolyLine.cc" line="130"/>
        <location filename="../FilePolyLine.cc" line="148"/>
        <source>Cannot find object for id %1 in saveFile!</source>
        <translation>Kann Objekt mit der Nummer %1 nicht finden!</translation>
    </message>
    <message>
        <source>PolyLine %1.pol</source>
        <translation type="vanished">PolygonaleLinie %1.pol</translation>
    </message>
    <message>
        <location filename="../FilePolyLine.hh" line="100"/>
        <source>Load/Save Poly Lines</source>
        <translation>Lade/Speichere Polygonale Linien</translation>
    </message>
</context>
</TS>
